import ChatScreen from "../components/chatScreen";
import styles from "./page.module.css";

export default function Home() {
  return (
    <main className={styles.main}>
      <ChatScreen />
    </main>
  );
}
