'use client';

import { useRef } from 'react';
import styles from './chatScreen.module.css';

function ChatScreen() {
    const sidebarRef = useRef<HTMLDivElement>(null);
    const checkboxRef = useRef<HTMLInputElement>(null);
    const linkTextRef = useRef<HTMLInputElement>(null);

    function toggleCollapse() {
        /*===== Handling an error when useRef has a minor change to return null (Really rare case tho...) =====*/
        if (!checkboxRef.current || !sidebarRef.current || !linkTextRef.current) {
            console.error('cardBodyRef or checkboxRef is null');
            return;
        }
        /*===== If checkbox is checked, add or remove class + rotate chevron icon =====*/
        if (checkboxRef.current.checked ) {
            sidebarRef.current.classList.add(`${styles.sidebarOpen}`);
            linkTextRef.current.classList.add(`${styles.showText}`);
        }
        else {
            sidebarRef.current.classList.remove(`${styles.sidebarOpen}`);
            linkTextRef.current.classList.remove(`${styles.showText}`);
        }
    }

    return (
        <section className={`${styles.dashboard}`}>
            <div className={`container ${styles.dashboard__container}`}>
                <nav className={`${styles.sidebar}`} ref={sidebarRef}>
                    <label className={`${styles.sidebar__button}`}>
                        <input type="checkbox" onChange={toggleCollapse} ref={checkboxRef} />
                    </label>
                    <ul className={`${styles.sidebar__items}`}>
                        <li className={`${styles.sidebar__item}`}>
                            <a className={`${styles.sidebar__link}`} href="#">
                                <i className="fa-regular fa-message"></i>
                                <span className={`${styles.hideText}`} ref={linkTextRef}>Notification</span>
                            </a>
                        </li>
                    </ul>
                </nav>
                <div className={`${styles.chatScreen}`}>
                    Chat Screen
                </div>
            </div>
        </section>
    )
}

export default ChatScreen;